/* eslint-disable no-undef */
const { createApp } = Vue;
import HeaderLLG from './components/header.js';
import Sidebar from './components/sidebar.js';
import Home from './sections/home.js';
import Minecraft from './sections/minecraft.js';

Neutralino.init();
const resNeuHome = await Neutralino.os.execCommand('./.scripts/gethome.sh');

const app = createApp({
	data() {
		return {
			section: 'home'
		};
	},
	template: `
        <HeaderLLG />
        <div class="container-fluid principal-content">
            <div class="row">
                <Sidebar v-model:section="section" />
                <div class="col-sm p-3">
                    <Home v-if="section === 'home'" />
                    <Minecraft v-if="section === 'minecraft'" />
                </div>
            </div>
        </div>
    `,
	components: {
		HeaderLLG,
		Sidebar,
		Home,
		Minecraft
	}
});

app.config.globalProperties.$neutralino = Neutralino;
app.config.globalProperties.$serverStatus = 'https://patojad.mooo.com:3030/status';
app.config.globalProperties.$home = resNeuHome.stdOut.replace(/(\r\n|\n|\r)/gm, '');

app.mount('#app');
