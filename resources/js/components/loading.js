export default {
    props: {
        title: String
    },
    template: `
    <div class='loader-cover'>
        <div class="loader">
            <div class="inner one"></div>
            <div class="inner two"></div>
            <div class="inner three"></div>
            <p class="text-center loading-text">{{title}}</p>
        </div>
    </div>
    `
};