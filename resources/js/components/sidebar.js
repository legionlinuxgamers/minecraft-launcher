export default {
    props: {
        section: String
    },
    template: `
    <div class="col-sm-auto sticky-top sidebar">
        <div class="d-flex flex-sm-column flex-row flex-nowrap align-items-center sticky-top">
            <ul class="nav nav-pills nav-flush flex-sm-column flex-row flex-nowrap mb-auto mx-auto text-center align-items-center">
                <li class="nav-item">
                    <a href="#" @click="$emit('update:section', 'home');" class="nav-link py-3 px-2" title="" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-original-title="Home">
                        <img src="/img/logo.svg" class="img-fluid sidebar-item" />
                    </a>
                    <a href="#" @click="$emit('update:section', 'minecraft');" class="nav-link py-3 px-2" title="" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-original-title="Minexraft">
                        <img src="/img/minecraft/logo.png" class="img-fluid sidebar-item" />
                    </a>
                </li>
            </ul>
            
        </div>
    </div>
    `
};