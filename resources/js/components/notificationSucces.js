const { onMounted } = Vue;

export default {
    props: {
        title: String
    },
    setup() {
        onMounted(() => {
            const toast = new bootstrap.Toast(document.getElementById('alert-succes'));
            toast.show();
        });
    },
    template: `
    <div class="position-fixed bottom-0 end-0 p-3">
        <div class="toast align-items-center text-white bg-success border-0" id="alert-succes" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {{title}}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
        </div>
    </div>
    `
};