#!/usr/bin/env bash

cd "/home/"$USER

if [ -d ".llg" ]; then
  echo "Existe"
else
  mkdir ".llg"
fi

cd ".llg/"

if [ -d "config" ]; then
  echo "Existe"
else
  mkdir "config"
fi

cd "config/"

if [ -d "minecraft" ]; then
  echo "Existe"
else
  mkdir "minecraft"
fi

cd "minecraft/"

if [ -f "userdata.json" ]; then
  echo "Existe"
else
  echo '{"installed":true,"user":"Linuxnauta","localVersion":"0.0.0"}' >"userdata.json"
fi
